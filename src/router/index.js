import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Home from '../views/Home'
import Laporan from '../views/Laporan'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  // path alat
  {
    path : '/home',
    name : 'Home',
    component: Home
  },
  {
    path : '/laporan',
    name : 'Laporan',
    component: Laporan
  },
  {
    path : '/logistik/alat/read',
    name : 'AlatRead',
    component : () => import('../views/logistik/alat/Read.vue')
  },
  {
    path : '/logistik/alat/insert',
    name : 'AlatInsert',
    component : () => import('../views/logistik/alat/Insert.vue')
  },
  {
    path : '/logistik/alat/update',
    name : 'AlatUpdate',
    component : () => import('../views/logistik/alat/Update.vue')
  },

  // path anggota
  {
    path : '/logistik/anggota/read',
    name : 'Read',
    component : () => import('../views/logistik/anggota/Read.vue')
  },
  {
    path : '/logistik/anggota/insert',
    name : 'Insert',
    component : () => import('../views/logistik/anggota/Insert.vue')
  },
  {
    path : '/logistik/anggota/update',
    name : 'Update',
    component : () => import('../views/logistik/anggota/Update.vue')
  },

  // path divisi
  {
    path : '/logistik/divisi/read',
    name : 'DivisiRead',
    component : () => import('../views/logistik/divisi/Read.vue')
  },
  {
    path : '/logistik/divisi/insert',
    name : 'DivisiInsert',
    component : () => import('../views/logistik/divisi/Insert.vue')
  },
  {
    path : '/logistik/divisi/update',
    name : 'DivisiUpdate',
    component : () => import('../views/logistik/divisi/Update.vue')
  },
  {
    path : '/peminjam/pinjam',
    name : 'Pinjam',
    component : () => import('../views/peminjam/Pinjam.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
